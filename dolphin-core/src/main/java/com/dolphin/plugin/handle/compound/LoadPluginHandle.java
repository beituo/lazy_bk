package com.dolphin.plugin.handle.compound;

import com.dolphin.plugin.PluginInfo;
import com.dolphin.plugin.handle.ApplicationContextPluginHandle;
import com.dolphin.plugin.handle.ClassHandle;
import com.dolphin.plugin.handle.MapperHandle;
import com.dolphin.plugin.handle.base.BasePluginHandle;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class LoadPluginHandle implements BasePluginHandle, ApplicationContextAware {
    ApplicationContext applicationContext;

    List<BasePluginHandle> pluginRegisterList = Collections.synchronizedList(new ArrayList<>());
    @Override
    public void initialize() throws Exception {
        pluginRegisterList.clear();
        pluginRegisterList.add(new ClassHandle());
        pluginRegisterList.add(new MapperHandle());
        pluginRegisterList.add(new ApplicationContextPluginHandle());
        for (BasePluginHandle pluginHandle : pluginRegisterList) {
            pluginHandle.initialize();
        }
    }

    @Override
    public void registry(PluginInfo plugin) throws Exception {
        for (BasePluginHandle pluginHandle : pluginRegisterList) {
            pluginHandle.registry(plugin);
        }
    }

    @Override
    public void unRegistry(PluginInfo plugin) throws Exception {
        try {
            for (BasePluginHandle pluginHandle : pluginRegisterList) {
                pluginHandle.unRegistry(plugin);
            }
        } finally {
            plugin.getClassList().clear();
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
