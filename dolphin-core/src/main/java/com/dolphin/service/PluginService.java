package com.dolphin.service;

import com.dolphin.commons.Pager;
import com.dolphin.commons.ResponseBean;
import com.dolphin.model.Plugin;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @description PluginService
 * @author dolphin
 * @date 2021/11/15 10:23
 */
public interface PluginService {
    /**
     * @description  添加插件
     * @param multiFile  multiFile
     * @return boolean
     * @author dolphin
     */
    ResponseBean addPlugin(MultipartFile multiFile);

    /**
     * @description 插件列表
     * @author dolphin
     */
    Pager<Plugin> list(Pager<Plugin> pager);

    /**
     * @description 卸载插件
     * @return boolean
     * @author dolphin
     */
    boolean del(String id);

    /**
     * @description 获取所有插件
     * @author dolphin
     */
    List<Plugin> getAll();

    boolean startPlugin(String id);

    boolean stopPlugin(String id);

    Plugin getById(String id);

    Plugin getByName(String id);
}
