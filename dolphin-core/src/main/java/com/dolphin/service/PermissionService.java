package com.dolphin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dolphin.commons.Pager;
import com.dolphin.model.Permission;
import com.dolphin.model.RolePermission;

import java.util.List;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-09-22 23:36
 * 个人博客地址：https://www.nonelonely.com
 */
public interface PermissionService extends IService<Permission> {
    /**
     * 重新加载权限
     */
    void updatePermission();

    /**
     * 更新权限
     */
    void updatePermission(List<Permission> permissions);

    /**
     * 获取角色资源
     */
    List<Permission> selectPermissionByRoleId(Long roleId);

    /**
     * 增加角色资源
     */
    void addPermissionByRoleId(RolePermission rolePermission);

    int changeEnable(Permission permission);

    Pager<Permission> list(Pager<Permission> pager);

    void removePluginSystemPermissionList(List<Permission> permissions);
}
