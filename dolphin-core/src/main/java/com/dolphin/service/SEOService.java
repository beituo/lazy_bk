package com.dolphin.service;

/**
 * @description SEOService
 * @author dolphin
 * @date 2021/11/15 10:25
 */
public interface SEOService {

    String createSiteMapXmlContent();
}
