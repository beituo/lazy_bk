package com.dolphin.service;


import com.dolphin.commons.Pager;
import com.dolphin.model.AccessLogs;

import java.util.HashMap;
import java.util.List;

public interface AccessLogsService {


    /**
     * 增加访问
     *
     * @param accessLogs accessLogs
     */
    void addAccess(AccessLogs accessLogs);

    /**
     * 获取当周访问数据
     *
     * @return
     */
    HashMap<String, List<Object>> getAccessCountByWeek();

    List<HashMap<String, Object>> getAccessCountBySysGroup();

    Pager<AccessLogs> list(Pager<AccessLogs> pager);
}
