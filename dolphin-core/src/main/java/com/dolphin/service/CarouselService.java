package com.dolphin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dolphin.commons.Pager;
import com.dolphin.model.Carousel;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-09-06 23:03
 * 个人博客地址：https://www.nonelonely.com
 */
public interface CarouselService extends IService<Carousel> {

    Pager<Carousel> list(Pager<Carousel> pager);
}
