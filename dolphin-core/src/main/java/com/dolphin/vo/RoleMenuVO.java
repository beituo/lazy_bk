package com.dolphin.vo;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-10-03 0:07
 * 个人博客地址：https://www.nonelonely.com
 */
public class RoleMenuVO {

    private String name;

    private String value;

    private boolean selected;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
