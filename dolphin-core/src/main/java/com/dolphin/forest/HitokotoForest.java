package com.dolphin.forest;

import com.dtflys.forest.annotation.Get;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-10-16 11:08
 * 个人博客地址：https://www.nonelonely.com
 */
public interface HitokotoForest {

    @Get("${0}")
    String getHitokoto(String url);
}
