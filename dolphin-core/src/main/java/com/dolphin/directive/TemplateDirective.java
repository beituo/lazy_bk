package com.dolphin.directive;

import java.lang.annotation.*;

/**
 * Custom TemplateDirective,Used to identify TemplateDirective
 * @author dolphin
 */
@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TemplateDirective {
    String value();
}
