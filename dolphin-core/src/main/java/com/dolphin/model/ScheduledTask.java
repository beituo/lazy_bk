package com.dolphin.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * @author nonelonely
 * @date 2020/04/15
 */
@ApiModel(value = "p_scheduled_task-定时任务", description = "p_scheduled_task-定时任务")
@TableName("p_scheduled_task")
public class ScheduledTask implements Serializable {
    // 主键ID
    @ApiModelProperty(value = "定时任务ID", name = "id", example = "1")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "任务key值（使用bean名称）", name = "taskKey", example = "1")
    private String taskKey;

    @ApiModelProperty(value = "任务描述", name = "taskDesc", example = "1")
    private String taskDesc;

    @ApiModelProperty(value = "任务表达式", name = "taskCron", example = "1")
    private String taskCron;
    @ApiModelProperty(value = "创建时间", name = "createTime")
    private Date createTime;

    @ApiModelProperty(value = "更新时间", name = "updateTime")
    private Date updateTime;

    @ApiModelProperty(value = "添加人", name = "createdBy")
    private Long createBy;
    @ApiModelProperty(value = "更新人", name = "updatedBy")
    private Long updateBy;

    // 程序初始化是否启动 1 是 0 否
    @ApiModelProperty(value = "程序初始化是否启动", name = "initStartFlag")
    private Integer initStartFlag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaskKey() {
        return taskKey;
    }

    public void setTaskKey(String taskKey) {
        this.taskKey = taskKey;
    }

    public String getTaskDesc() {
        return taskDesc;
    }

    public void setTaskDesc(String taskDesc) {
        this.taskDesc = taskDesc;
    }

    public String getTaskCron() {
        return taskCron;
    }

    public void setTaskCron(String taskCron) {
        this.taskCron = taskCron;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Integer getInitStartFlag() {
        return initStartFlag;
    }

    public void setInitStartFlag(Integer initStartFlag) {
        this.initStartFlag = initStartFlag;
    }

    @TableField(exist = false)
    private Boolean startFlag;

    public Boolean getStartFlag() {
        return startFlag;
    }

    public void setStartFlag(Boolean startFlag) {
        this.startFlag = startFlag;
    }
}
