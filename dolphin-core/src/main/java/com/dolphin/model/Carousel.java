package com.dolphin.model;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * @author nonelonely
 * @date 2020/11/16
 */

@ApiModel(value = "Carousel-轮播图功能", description = "轮播图功能")
@TableName("p_carousel")
public class Carousel implements Serializable {

    @ApiModelProperty(value = "轮播图ID", name = "id", example = "1")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "轮播图标题", name = "title", example = "Hello World")
    private String title;

    @ApiModelProperty(value = "轮播图图片路径", name = "imageUrl", example = "http://www.wubaobaotools.cn")
    private String imageUrl;

    @ApiModelProperty(value = "轮播图跳转路径", name = "url", example = "http://www.wubaobaotools.cn")
    private String url;

    @ApiModelProperty(value = "轮播图过期时间", name = "overTime")
    private Date overTime;

    @ApiModelProperty(value = "轮播图备注", name = "remark")
    private String remark;

    @ApiModelProperty(value = "创建时间", name = "createTime")
    private Date createTime;

    @ApiModelProperty(value = "更新时间", name = "updateTime")
    private Date updateTime;

    @ApiModelProperty(value = "添加人", name = "createdBy")
    private Long createBy;
    @ApiModelProperty(value = "更新人", name = "updatedBy")
    private Long updateBy;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getOverTime() {
        return overTime;
    }

    public void setOverTime(Date overTime) {
        this.overTime = overTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }
}
