package com.dolphin.controller.admin;

import com.dolphin.base.BaseController;
import com.dolphin.commons.Constants;
import com.dolphin.commons.Pager;
import com.dolphin.commons.ResponseBean;
import com.dolphin.model.Category;
import com.dolphin.permission.ApiAuth;
import com.dolphin.service.CategoryService;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
public class CategoryController extends BaseController {
    private final Logger logger = LoggerFactory.getLogger(CategoryController.class);
    @Autowired
    private CategoryService categoryService;

    /**
     * 分类管理列表页
     *
     * @return String
     */
    @RequestMapping("/category")
    @ApiAuth(name = "分类管理", groupId = Constants.ADMIN_MENU_GROUP_CONTENT, permission = "admin:category:index")
    public String index() {
        return view("static/admin/pages/category/category_list.html");
    }

    /**
     * 添加分类
     *
     * @return String
     */
    @PostMapping("/category/add")
    @ResponseBody
    @ApiAuth(name = "添加分类", permission = "admin:category:add")
    public ResponseBean add(@RequestBody @Valid Category category) {
        Category categoryBySlug = categoryService.getBySlug(category.getSlug());
        if (categoryBySlug != null) {
            return ResponseBean.fail("访问地址别名重复!", null);
        }
        if (categoryService.add(category) > 0) {
            return ResponseBean.success("添加成功", null);
        }
        logger.error("分类添加失败: {}", category.toString());
        return ResponseBean.fail("添加失败", null);
    }


    /**
     * 分类管理列表数据
     *
     * @return String
     */
    @PostMapping("/category/list")
    @ResponseBody
    @ApiAuth(name = "分类管理列表数据", permission = "admin:category:list")
    public Pager<Category> list(@RequestBody Pager<Category> pager) {
        return categoryService.list(pager);
    }

    /**
     * 获取所有分类
     *
     * @return String
     */
    @GetMapping("/category/allList")
    @ResponseBody
    @ApiAuth(name = "获取所有分类", permission = "admin:category:allList")
    public ResponseBean allList() {
        return ResponseBean.success("获取成功", categoryService.allList(new Category()));
    }

    /**
     * 删除分类
     *
     * @return String
     */
    @PostMapping("/category/del")
    @ResponseBody
    @ApiAuth(name = "删除分类", permission = "admin:category:del")
    public ResponseBean del(@RequestBody String ids) {
        String[] idArr = ids.split(",");
        if (categoryService.del(idArr) > 0) {
            return ResponseBean.success("删除成功", null);
        }
        logger.error("分类删除失败: {}", ids);
        return ResponseBean.fail("删除失败", null);
    }

    /**
     * 更新分类
     *
     * @return String
     */
    @PostMapping("/category/update")
    @ResponseBody
    @ApiAuth(name = "更新分类", permission = "admin:category:update")
    public ResponseBean update(@RequestBody @Valid Category category) {
        if (category.getPid().equals(category.getId())) {
            return ResponseBean.fail("不可将当前分类设置为父级分类", null);
        }
        if (StringUtils.isBlank(category.getSlug())) {
            return ResponseBean.fail("访问地址别名不能为空", null);
        }
        Category categoryBySlug = categoryService.getBySlug(category.getSlug());
        if (categoryBySlug != null && !categoryBySlug.getId().equals(category.getId())) {
            return ResponseBean.fail("访问地址别名重复", null);
        }
        if (categoryService.update(category) > 0) {
            return ResponseBean.success("更新成功", null);
        }
        logger.error("分类更新失败: {}", category.toString());
        return ResponseBean.fail("更新失败", null);
    }

    /**
     * 更改状态
     *
     * @return String
     */
    @PostMapping("/category/changeStatus")
    @ResponseBody
    @ApiAuth(name = "更改分类状态", permission = "admin:category:changeStatus")
    public ResponseBean changeStatus(@RequestBody Category category) {
        if (categoryService.changeStatus(category) > 0) {
            return ResponseBean.success("修改成功", null);
        }
        logger.error("分类修改失败: {}", category.toString());
        return ResponseBean.fail("修改失败", null);
    }

    @GetMapping("/category/getById")
    @ResponseBody
    @ApiAuth(name = "获取某个分类", permission = "admin:category:getById")
    public ResponseBean getById(@ApiParam(name = "categoryId", value = "分类ID", required = true) @RequestParam("categoryId") String categoryId) {
        Category category = categoryService.getById(categoryId);
        return ResponseBean.success("success", category);
    }
}
