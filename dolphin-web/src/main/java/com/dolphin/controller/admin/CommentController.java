package com.dolphin.controller.admin;

import cn.hutool.http.HtmlUtil;
import com.dolphin.base.BaseController;
import com.dolphin.commons.Constants;
import com.dolphin.commons.Pager;
import com.dolphin.commons.ResponseBean;
import com.dolphin.model.Comment;
import com.dolphin.model.Permission;
import com.dolphin.model.User;
import com.dolphin.permission.ApiAuth;
import com.dolphin.service.ArticleService;
import com.dolphin.service.CommentService;
import com.dolphin.service.MailService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
public class CommentController extends BaseController {
    private final Logger logger = LoggerFactory.getLogger(TagController.class);
    @Autowired
    private CommentService commentService;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private MailService mailService;

    /**
     * 评论管理列表页
     *
     * @return String
     */
    @RequestMapping("/comment")
    @ApiAuth(name = "评论管理", permission = "admin:dolphin-comment:index", groupId = Constants.ADMIN_MENU_GROUP_CONTENT)
    public String index() {
        return view("static/admin/pages/comment/comment_list.html");
    }


    /**
     * 评论回复页
     *
     * @return String
     */
    @RequestMapping("/comment/reply/{id}")
    @ApiAuth(name = "评论回复页", permission = "admin:dolphin-comment:reply", type = Permission.ResType.NAV_LINK)
    public String reply(@PathVariable("id") String id, Model model) {
        model.addAttribute("comment", commentService.getById(Long.parseLong(id)));
        return view("static/admin/pages/comment/comment_reply.html");
    }

    /**
     * 评论管理列表数据
     *
     * @return String
     */
    @PostMapping("/comment/list")
    @ApiAuth(name = "评论管理列表数据", permission = "admin:dolphin-comment:list")
    @ResponseBody
    public Pager<Comment> list(@RequestBody Pager<Comment> pager) {
        User user = getUser();
        if (user.getRole().getCode().equals("contribute")) {
            return commentService.list(pager, user.getId().toString());
        }
        return commentService.list(pager, null);
    }

    /**
     * 删除评论
     *
     * @return String
     */
    @PostMapping("/comment/del")
    @ApiAuth(name = "删除评论", permission = "admin:dolphin-comment:del")
    @ResponseBody
    public ResponseBean del(@RequestBody String ids) {
        String[] idArr = ids.split(",");
        if (commentService.del(idArr) > 0) {
            return ResponseBean.success("删除成功", null);
        }
        logger.error("评论删除失败: {}", ids);
        return ResponseBean.fail("删除失败", null);
    }

    /**
     * 更改状态
     *
     * @return String
     */
    @PostMapping("/comment/changeStatus")
    @ApiAuth(name = "更改评论状态", permission = "admin:dolphin-comment:changeStatus")
    @ResponseBody
    public ResponseBean changeStatus(@RequestBody Comment comment) {
        if (commentService.changeStatus(comment) > 0) {
            return ResponseBean.success("操作成功", null);
        }
        logger.error("评论操作失败: {}", comment.toString());
        return ResponseBean.fail("操作失败", null);
    }


    @RequestMapping("/comment/reply")
    @ResponseBody
    @ApiAuth(name = "回复评论", permission = "admin:dolphin-comment:reply")
    public ResponseBean reply(@RequestBody @Valid Comment comment) {
        comment.setReadAvatar(false);
        User user = getUser();
        user.setReadAvatar(false);
        comment.setUserId(user.getId());
        comment.setAvatar(user.getAvatar());
        comment.setEmail(user.getEmail());
        comment.setUserName(user.getUserName());
        comment.setWebsite(user.getWebsite());
        comment.setStatus(Constants.COMMENT_STATUS_NORMAL);
        comment.setContent(HtmlUtil.filter(comment.getContent()));
        if (commentService.add(comment) > 0) {
            mailService.commentMailSend(comment);
            return ResponseBean.success("评论成功", comment);
        }
        return ResponseBean.fail("评论失败", null);
    }

}
