package com.dolphin.controller.admin;

import com.dolphin.base.BaseController;
import com.dolphin.commons.Constants;
import com.dolphin.model.Permission;
import com.dolphin.model.Photos;
import com.dolphin.permission.AdminMenu;
import com.dolphin.permission.ApiAuth;
import com.dolphin.service.PhotosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class PhotosController extends BaseController {

    @Autowired
    private PhotosService photosService;

    /**
     * @return java.lang.String
     * @description 相册管理页
     * @author dolphin
     */
    @GetMapping("/photos")
    @ApiAuth(name = "相册管理页", permission = "admin:photos:index",groupId = Constants.ADMIN_MENU_GROUP_CONTENT,type = Permission.ResType.NAV_LINK)
    public String adminPhotos() {
        return "/static/admin/pages/photos/photos-list.html";
    }

    /**
     * @return java.lang.String
     * @description 添加相册页
     * @author dolphin
     */
    @GetMapping("/photos/addPage")
    @ApiAuth(name = "添加相册页", permission = "admin:photos:addPage",type = Permission.ResType.NAV_LINK)
    public String addPhotosPage() {
        return "/static/admin/pages/photos/photos-add.html";
    }

    /**
     * 编辑相册页
     *
     * @return String
     */
    @GetMapping("/photos/editPage/{id}")
    @ApiAuth(name = "编辑相册页", permission = "admin:photos:editPage",type = Permission.ResType.NAV_LINK)
    public String editPage(@PathVariable("id") String id, Model model) {
        Photos photos = photosService.getById(Long.parseLong(id));
        model.addAttribute("photos", photos);
        return view("/static/admin/pages/photos/photos-edit.html");
    }


}
