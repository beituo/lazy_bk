package com.dolphin.controller.admin;

import com.dolphin.base.BaseController;
import com.dolphin.commons.Constants;
import com.dolphin.commons.Pager;
import com.dolphin.commons.ResponseBean;
import com.dolphin.file.FileDownLoad;
import com.dolphin.model.DataBaseBackup;
import com.dolphin.permission.ApiAuth;
import com.dolphin.service.DataBaseBackupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-10-05 23:27
 * 个人博客地址：https://www.nonelonely.com
 */
@Controller
@RequestMapping("/admin")
public class DataBaseBackupController extends BaseController {
    @Autowired
    private DataBaseBackupService dataBaseBackupService;

    @RequestMapping("/dataBaseBackup")
    @ApiAuth(name = "数据库备份管理", permission = "admin:dataBaseBackup:index", groupId = Constants.ADMIN_MENU_GROUP_SETTING)
    public String index(Model model) {
        return view("static/admin/pages/settings/dataBaseBackup.html");
    }


    @GetMapping("/dataBaseBackup/mysqlBackups")
    @ResponseBody
    @ApiAuth(name = "数据库备份管理", permission = "admin:dataBaseBackup:mysqlBackups")
    public ResponseBean mysqlBackups() throws Exception {
        // 调用备份
        Object systemMysqlBackups = dataBaseBackupService.mysqlBackups();
        return ResponseBean.success("备份成功", systemMysqlBackups);
    }

    @PostMapping("/dataBaseBackup/rollback")
    @ResponseBody
    @ApiAuth(name = "数据库还原", permission = "admin:dataBaseBackup:rollback")
    public ResponseBean rollback(@RequestBody Long id) throws Exception {
        // 根据id查询查询已有的信息
        DataBaseBackup smb = dataBaseBackupService.selectListId(id);
        // 恢复数据库
        Object rollback = dataBaseBackupService.rollback(smb);
        // 更新操作次数
        dataBaseBackupService.updateById(smb);
        return ResponseBean.success("还原成功", rollback);
    }

    /**
     * @param pager pager
     * @return
     * @description 数据库备份分页
     * @author dolphin
     */
    @PostMapping("/dataBaseBackup/list")
    @ApiAuth(name = "数据库备份分页", permission = "admin:dataBaseBackup:list")
    @ResponseBody
    public Pager<DataBaseBackup> list(@RequestBody Pager<DataBaseBackup> pager) {
        return dataBaseBackupService.list(pager);
    }

    /**
     * 删除数据库备份
     *
     * @return String
     */
    @PostMapping("/dataBaseBackup/del")
    @ResponseBody
    @ApiAuth(name = "删除数据库备份", permission = "admin:dataBaseBackup:del")
    public ResponseBean del(@RequestBody String id) {
        if (dataBaseBackupService.removeById(id)) {
            return ResponseBean.success("删除成功", null);
        }
        return ResponseBean.fail("删除失败", null);
    }

    /**
     * 下载备份文件
     *
     * @param response response
     * @param id       id
     * @return String
     */
    @GetMapping("/dataBaseBackup/download/{id}")
    @ApiAuth(name = "下载数据库备份", permission = "admin:dataBaseBackup:download")
    public String downloadDataBaseBackup(HttpServletResponse response, @PathVariable("id") String id) {
        DataBaseBackup dataBaseBackup = dataBaseBackupService.getById(Long.parseLong(id));
        File file = new File(dataBaseBackup.getBackupsPath() + dataBaseBackup.getBackupsName());
        if (file != null && file.exists()) {
            FileDownLoad.fileDownLoad(response, file);
        }
        return null;
    }


}
