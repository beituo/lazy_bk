package com.dolphin.controller.admin;

import com.dolphin.base.BaseController;
import com.dolphin.commons.Constants;
import com.dolphin.commons.Pager;
import com.dolphin.commons.ResponseBean;
import com.dolphin.model.Article;
import com.dolphin.model.Permission;
import com.dolphin.model.User;
import com.dolphin.permission.ApiAuth;
import com.dolphin.service.ArticleService;
import com.dolphin.service.CategoryService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 文章
 */
@Controller
@RequestMapping("/admin")
public class ArticleController extends BaseController {
    private final Logger logger = LoggerFactory.getLogger(ArticleController.class);
    @Autowired
    private ArticleService articleService;

    @Autowired
    private CategoryService categoryService;


    @RequestMapping("/article")
    @ApiAuth(name = "文章管理", permission = "admin:article:index", groupId = Constants.ADMIN_MENU_GROUP_CONTENT)
    public String index(Model model) {
        return view("static/admin/pages/article/article_list.html");
    }

    @RequestMapping("/article/addPage")
    @ApiAuth(name = "增加文章页面", permission = "admin:article:addPage", type = Permission.ResType.NAV_LINK)
    public String addPage() {
        return view("static/admin/pages/article/article_create.html");
    }

    @RequestMapping("/article/updatePage/{id}")
    @ApiAuth(name = "更新文章页面", permission = "admin:article:updatePage", type = Permission.ResType.NAV_LINK)
    public String updatePage(@PathVariable("id") String id, Model model) {
        Article article = articleService.getById(id);
        model.addAttribute("article", article);
        return view("/static/admin/pages/article/article_update.html");
    }

    /**
     * 添加文章
     *
     * @return String
     */
    @PostMapping("/article/add")
    @ResponseBody
    @ApiAuth(name = "添加文章", permission = "admin:article:add")
    public ResponseBean add(@RequestBody @Valid Article article) {
        User user = getUser();
        article.setUserId(user.getId());
        if (user.getRole().getCode().equals("contribute")) {
            article.setStatus(Constants.ARTICLE_STATUS_AUDIT);
        }
        Article articleBySlug = articleService.getBySlug(article.getSlug(), Constants.ARTICLE_TYPE_ARTICLE);
        if (articleBySlug != null) {
            return ResponseBean.fail("访问地址别名重复!", null);
        }
        if (articleService.add(article) > 0) {
            return ResponseBean.success("添加成功", article);
        }
        logger.error("文章添加失败: {}", article.toString());
        return ResponseBean.fail("添加失败", null);
    }

    /**
     * 更新文章
     *
     * @return String
     */
    @PostMapping("/article/update")
    @ResponseBody
    @ApiAuth(name = "更新文章", permission = "admin:article:update")
    public ResponseBean update(@RequestBody @Valid Article article) {
        if (StringUtils.isBlank(article.getSlug())) {
            return ResponseBean.fail("访问地址别名不能为空", null);
        }
        Article articleBySlug = articleService.getBySlug(article.getSlug(), Constants.ARTICLE_TYPE_ARTICLE);
        if (articleBySlug != null && !articleBySlug.getId().equals(article.getId())) {
            return ResponseBean.fail("访问地址别名重复", null);
        }
        if (articleService.update(article) > 0) {
            return ResponseBean.success("更新成功", article);
        }
        logger.error("文章更新失败: {}", article.toString());
        return ResponseBean.fail("更新失败", null);
    }

    /**
     * 文章管理列表数据
     *
     * @return Pager<Article>
     */
    @ApiAuth(name = "文章管理列表数据", permission = "admin:article:list")
    @PostMapping("/article/list")
    @ResponseBody
    public Pager<Article> list(@RequestBody Pager<Article> pager) {
        User user = getUser();
        if (user.getRole().getCode().equals("contribute")) {
            pager.getForm().setUserId(user.getId());
        }
        return articleService.list(pager);
    }


    /**
     * 更改置顶状态
     *
     * @return String
     */
    @ApiAuth(name = "更改置顶状态", permission = "admin:article:changeTopStatus")
    @PostMapping("/article/changeTopStatus")
    @ResponseBody
    public ResponseBean changeTopStatus(@RequestBody Article article) {
        if (articleService.changeTopStatus(article) > 0) {
            return ResponseBean.success("修改成功", null);
        }
        logger.error("文章修改失败: {}", article.toString());
        return ResponseBean.fail("修改失败", null);
    }


    /**
     * 更改文章是否可以评论
     *
     * @return String
     */
    @ApiAuth(name = "更改文章是否可以评论", permission = "admin:article:changeCommentStatus")
    @PostMapping("/article/changeCommentStatus")
    @ResponseBody
    public ResponseBean changeCommentStatus(@RequestBody Article article) {
        if (articleService.changeCommentStatus(article) > 0) {
            return ResponseBean.success("修改成功", null);
        }
        logger.error("文章修改失败: {}", article.toString());
        return ResponseBean.fail("修改失败", null);
    }

    /**
     * 更改文章状态
     *
     * @return String
     */
    @ApiAuth(name = "更改文章状态", permission = "admin:article:changeStatus")
    @PostMapping("/article/changeStatus")
    @ResponseBody
    public ResponseBean changeStatus(@RequestBody Article article) {
        if (articleService.changeStatus(article) > 0) {
            return ResponseBean.success("修改成功", null);
        }
        logger.error("文章修改失败: {}", article.toString());
        return ResponseBean.fail("修改失败", null);
    }

    /**
     * 删除文章
     *
     * @param ids ids
     * @return ResponseBean
     */
    @ApiAuth(name = "删除文章", permission = "admin:article:del")
    @PostMapping("/article/del")
    @ResponseBody
    public ResponseBean del(@RequestBody String ids) {
        String[] idArr = ids.split(",");
        if (articleService.del(idArr) > 0) {
            return ResponseBean.success("删除成功", null);
        }
        logger.error("文章删除失败: {}", ids);
        return ResponseBean.fail("删除失败", null);
    }

}
