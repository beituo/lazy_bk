package com.dolphin.controller.admin;

import com.dolphin.base.BaseController;
import com.dolphin.commons.Constants;
import com.dolphin.commons.Pager;
import com.dolphin.commons.ResponseBean;
import com.dolphin.model.Permission;
import com.dolphin.model.Plugin;
import com.dolphin.permission.ApiAuth;
import com.dolphin.service.PluginService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;

/**
 * @description 插件
 * @author dolphin
 * @date 2021/8/13 13:46
 */
@RequestMapping("/admin")
@Controller
public class PluginController extends BaseController {
    private final Logger logger = LoggerFactory.getLogger(PluginController.class);

    @Autowired
    private PluginService pluginService;

    /**
     * @description 插件页
     * @author dolphin
     */
    @RequestMapping("/plugin")
    @ApiAuth(name = "插件管理", permission = "admin:plugin:index",groupId = Constants.ADMIN_MENU_GROUP_PLUGIN,type = Permission.ResType.NAV_LINK)
    public String index() {
        return view("static/admin/pages/plugin/plugin_list.html");
    }

    /**
     * 安装插件
     * @return String
     */
    @PostMapping("/plugin/addPlugin")
    @ResponseBody
    @ApiAuth(name = "安装插件", permission = "admin:plugin:addPlugin")
    public ResponseBean addPlugin(HttpServletRequest request) {
        try{
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile multiFile = multipartRequest.getFile("file");
            if (multiFile == null){
                return ResponseBean.fail("文件不能为空", null);
            }
            return pluginService.addPlugin(multiFile);
        }catch (Exception e){
            logger.error("插件安装失败: {}", e.getMessage());
            return ResponseBean.fail("插件安装失败", e.getMessage());
        }
    }

    /**
     * 插件列表数据
     * @return String
     */
    @PostMapping("/plugin/list")
    @ResponseBody
    @ApiAuth(name = "插件列表数据", permission = "admin:plugin:list")
    public Pager<Plugin> list(@RequestBody Pager<Plugin> pager) {
        return pluginService.list(pager);
    }

    /**
     * 删除插件
     * @return String
     */
    @PostMapping("/plugin/del")
    @ResponseBody
    @ApiAuth(name = "删除插件", permission = "admin:plugin:del")
    public ResponseBean del(@RequestBody String id) {
        if (pluginService.del(id)) {
            return ResponseBean.success("卸载成功", null);
        }
        logger.error("卸载失败: {}", id);
        return ResponseBean.fail("卸载失败", null);
    }

    /**
     * 启用插件
     * @return String
     */
    @PostMapping("/plugin/startPlugin")
    @ResponseBody
    @ApiAuth(name = "启用插件", permission = "admin:plugin:startPlugin")
    public ResponseBean startPlugin(@RequestBody String id) {
        if (pluginService.startPlugin(id)) {
            return ResponseBean.success("启用成功", null);
        }
        logger.error("启用失败: {}", id);
        return ResponseBean.fail("启用失败", null);
    }

    @PostMapping("/plugin/stopPlugin")
    @ResponseBody
    @ApiAuth(name = "停用插件", permission = "admin:plugin:stopPlugin")
    public ResponseBean stopPlugin(@RequestBody String id) {
        if (pluginService.stopPlugin(id)) {
            return ResponseBean.success("禁用成功", null);
        }
        logger.error("禁用失败: {}", id);
        return ResponseBean.fail("禁用失败", null);
    }
}
