package com.dolphin.mapper;

import com.dolphin.model.Photos;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface PhotosMapper {
    void dropPhotosTable();

    void createPhotosTableForMysql();

    void createPhotosTableForSqlite();

    List<Photos> getList(@Param("page") int page, @Param("size") int size, @Param("name") String name);

    Long getTotal(@Param("name") String name);

    void addPhotos(Photos photos);

    int del(long photosId);

    Photos getById(long id);

    int update(Photos photos);

}
