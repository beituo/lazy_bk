package com.dolphin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dolphin.model.Permission;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-09-22 23:34
 * 个人博客地址：https://www.nonelonely.com
 */
@Mapper
@Component
public interface PermissionMapper extends BaseMapper<Permission> {

    List<Permission> selectPermissionByRoleId(Long roleId);

    int changeEnable(Permission permission);

    void deletePermissionByNotUrl(List<Permission> permissions);

    void deletePermissionByUrl(List<Permission> permissions);

}
