package com.dolphin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dolphin.commons.Pager;
import com.dolphin.mapper.WordMapper;
import com.dolphin.model.Word;
import com.dolphin.service.WordService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-10-24 22:24
 * 个人博客地址：https://www.nonelonely.com
 */
@Service
public class WordServiceImpl extends ServiceImpl<WordMapper, Word> implements WordService {
    private final Logger log = LoggerFactory.getLogger(WordServiceImpl.class);

    /**
     * 获取分页列表数据
     *
     * @param pager 查询实例
     * @return 返回分页数据
     */
    public Pager<Word> list(Pager<Word> pager) {
        PageHelper.startPage(pager.getPageIndex(), pager.getPageSize());
        QueryWrapper<Word> userQueryWrapper = new QueryWrapper<>(pager.getForm());
        List<Word> taskBeanList = this.list(userQueryWrapper);
        PageInfo<Word> pageInfo = new PageInfo<>(taskBeanList);
        pager.setTotal(pageInfo.getTotal());
        pager.setData(pageInfo.getList());
        pager.setCode(Pager.SUCCESS_CODE);
        return pager;
    }

    public Word getWordRandom(int limit) {
        return this.baseMapper.findRandWords(limit);
    }
}
