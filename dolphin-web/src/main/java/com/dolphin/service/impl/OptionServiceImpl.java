package com.dolphin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dolphin.commons.DynamicDataSource;
import com.dolphin.commons.Pager;
import com.dolphin.mapper.OptionMapper;
import com.dolphin.model.Option;
import com.dolphin.service.OptionService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Service
@Transactional
public class OptionServiceImpl extends ServiceImpl<OptionMapper, Option> implements OptionService {
    private static final CacheManager cacheManager = CacheManager.newInstance();
    @Autowired
    private OptionMapper optionMapper;

    /**
     * 根据key获取option
     *
     * @param key key
     * @return Option
     */
    @Transactional(readOnly = true)
    @Override
    public Option getOptionByKey(String key) {
        return optionMapper.getOptionByKey(key);
    }

    /**
     * 根据key更新value
     *
     * @param option option
     * @return int
     */
    @Override
    public int updateValueByKey(Option option) {
        int count = optionMapper.updateValueByKey(option);
        if (count > 0) {
            Ehcache cache = cacheManager.getEhcache("optionData");
            cache.put(new Element(option.getOptionKey(), option.getOptionValue()));
        }
        return count;
    }

    /**
     * 批量添加或更新option
     *
     * @param options options
     * @return int
     */
    @Override
    public int addOrUpdateOptions(List<Option> options) {
        AtomicReference<Integer> count = new AtomicReference<>(0);
        options.forEach(o -> {
            Option optionByKey = optionMapper.getOptionByKey(o.getOptionKey());
            if (optionByKey != null) {
                count.updateAndGet(v -> v + optionMapper.updateValueByKey(o));
            } else {
                if (DynamicDataSource.dataSourceType.equals("mysql")) {
                    count.updateAndGet(v -> v + optionMapper.addOption(o));
                } else {
                    int maxId = optionMapper.getMaxId() + 1;
                    o.setId(Long.parseLong(String.valueOf(maxId)));
                    count.updateAndGet(v -> v + optionMapper.addOptionBySqlite(o));
                }
            }
            Ehcache cache = cacheManager.getEhcache("optionData");
            cache.put(new Element(o.getOptionKey(), o.getOptionValue()));
        });
        return count.get();
    }

    @Override
    public void initOptionCache() {
        List<Option> options = optionMapper.getStartOption();
        Ehcache cache = cacheManager.getEhcache("optionData");
        cache.removeAll();
        options.forEach(r -> cache.put(new Element(r.getOptionKey(), r.getOptionValue())));
    }

    @Override
    public Pager<Option> list(Pager<Option> pager) {
        PageHelper.startPage(pager.getPageIndex(), pager.getPageSize());
        QueryWrapper<Option> userQueryWrapper = new QueryWrapper<>(pager.getForm());
        List<Option> categories = this.list(userQueryWrapper);
        PageInfo<Option> pageInfo = new PageInfo<>(categories);
        pager.setTotal(pageInfo.getTotal());
        pager.setData(pageInfo.getList());
        pager.setCode(Pager.SUCCESS_CODE);
        return pager;
    }
}
