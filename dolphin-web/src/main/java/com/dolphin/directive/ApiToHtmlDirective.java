package com.dolphin.directive;

import com.dolphin.forest.HitokotoForest;
import com.jfinal.template.Env;
import com.jfinal.template.expr.ast.ExprList;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@TemplateDirective("apiToHtml")
@Component
public class ApiToHtmlDirective extends BaseDirective {

    private static HitokotoForest hitokotoForest;

    @Autowired
    public void setHitokotoForest(HitokotoForest hitokotoForest) {
        ApiToHtmlDirective.hitokotoForest = hitokotoForest;
    }

    public void setExprList(ExprList exprList) {
        super.setExprList(exprList);
    }

    @Override
    public void exec(Env env, Scope scope, Writer writer) {
        int i = 0;
        while (i < 1) {
            try {
                String url = getParam(0, scope).toString();
                write(writer, hitokotoForest.getHitokoto(url));
                i++;
            } catch (Exception e) {
                try {
                    Thread.sleep(1);
                } catch (Exception e1) {

                }

            }
        }
    }
}
