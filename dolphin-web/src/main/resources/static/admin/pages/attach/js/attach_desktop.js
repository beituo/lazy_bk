let table, form, layer, layPage, flow, upload, laytpl, $, toast;
let pageIndex = 1, pageSize = 8;
layui.use(['table', 'form', 'layer', 'laypage', 'flow', 'upload', 'laytpl', 'jquery', 'toast'], function () {
    table = layui.table;
    form = layui.form;
    layer = layui.layer;
    layPage = layui.laypage;
    flow = layui.flow;
    upload = layui.upload;
    laytpl = layui.laytpl;
    $ = layui.jquery;
    toast = layui.toast;
    queryTable();

    // 查询
    $("#queryBtn").click(function () {
        queryTable();
    });
    // 上传图片
    $("#uploadBtn").click(function () {
        layer.open({
            title: "选择图片",
            type: 2,
            offset: '10%',
            area: parent.layerArea($("html")[0].clientWidth, 700, 500),
            shadeClose: true,
            anim: 1,
            move: true,
            content: '/admin/attach/img'
        });
    });
    $("#tableBox").on("click", ".img-box", function () {
        var src = $(this).children("img").attr("src");
        var loading = layer.load(1, {
            shade: [0.1,'#fff'] //0.1透明度的白色背景
        });
        $.get("/admin/option/getOptionByKey", {
            optionKey: 'admin_deskTop_backImage',
        }, function (returnData) {
            if (returnData.code === 200) {
                var dskTopImage = JSON.parse(returnData.data.optionValue);
                dskTopImage = dskTopImage.filter((item) => item != src)
                dskTopImage.unshift(src);
                $.ajax({
                    type: "POST",
                    url: "/admin/option/update",
                    contentType: "application/json",
                    data: JSON.stringify({
                        optionKey: 'admin_deskTop_backImage',
                        optionValue: JSON.stringify(dskTopImage)
                    }),
                    success: function (data) {
                        if (data.code === 200) {
                            parent.Win10.setBgUrl();
                            queryTable();
                        } else {
                            layer.msg(data.msg, {icon: 2});
                        }
                    }
                });
            }
            layer.close(loading);
        });

    });
});

/**
 * 初始化页面数据
 */
function queryTable() {
    $.get("/admin/option/getOptionByKey", {
        optionKey: 'admin_deskTop_backImage',
    }, function (returnData) {
        if (returnData.code === 200) {
            var dskTopImage = JSON.parse(returnData.data.optionValue);
            laytpl($("#tableTpl").html()).render(dskTopImage, function (html) {
                $("#tableBox").html(html);
            });
        }
    });
}

/**
 * 选择图片
 * @param path
 * @param name
 */
function selectImg(path, name) {
    $.get("/admin/option/getOptionByKey", {
        optionKey: 'admin_deskTop_backImage',
    }, function (returnData) {
        if (returnData.code === 200) {
            var dskTopImage = JSON.parse(returnData.data.optionValue);
            dskTopImage = dskTopImage.filter((item) => item != path)
            dskTopImage.unshift(path);
            $.ajax({
                type: "POST",
                url: "/admin/option/update",
                contentType: "application/json",
                data: JSON.stringify({
                    optionKey: 'admin_deskTop_backImage',
                    optionValue: JSON.stringify(dskTopImage)
                }),
                success: function (data) {
                    if (data.code === 200) {
                        parent.Win10.setBgUrl();
                        queryTable();
                    } else {
                        layer.msg(data.msg, {icon: 2});
                    }
                }
            });
        }
    });
}
