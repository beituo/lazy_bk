var table, treeTable, layPage, form,$,toast;
let pageIndex = 1, pageSize = 20;
layui.use(['table', 'treeTable', 'laypage', 'form','toast'], function () {
    table = layui.table;
    treeTable = layui.treeTable;
    layPage = layui.laypage;
    form = layui.form;
    toast = layui.toast;
    initPage();
});


/**
 * 页面初始化事件
 */
function initPage() {
    queryTable();

    // 查询
    $("#queryBtn").click(function () {
        queryTable();
    });

    // 添加
    $("#addBtn").click(function () {
        add();
    });
}
/**
 * 查询表格数据
 */
function queryTable() {
    table.render({
        elem: '#tableBox',
        url: '/admin/permission/list',
        method: 'post',
        headers: {'Content-Type': 'application/json'},
        contentType: 'application/json',
        title: '角色列表',
        totalRow: false,
        limit: 10,
        where: {
            form: {
                name: $("#name").val() == ''?null:$("#name").val()
            }
        },
        cols: [[
            {field: 'id', title: 'ID', width: 80},
            {field: 'name', minWidth: 160,title: '权限名'},
            {field: 'url', minWidth: 200,title: '权限链接'},
            {field: 'permissionValue', minWidth: 200,title: '权限值'},
            {
                field: 'permissionType', minWidth: 100,title: '权限类型', templet: function (d) {
                    let html;
                    if (d.permissionType == 'NAV_LINK') {
                        html = "页面";
                    } else {
                        html = "API";
                    }
                    return html;
                }
            },
            {
                field: 'enable', minWidth: 100,title: '是否启用', templet: function (d) {
                    let html;
                    if (d.enable == 0) {
                        html = "<input type='checkbox' name='enable' lay-filter='enable' lay-skin='switch' value='" + d.id + "' lay-text='正常|禁用' checked>";
                    } else {
                        html = "<input type='checkbox' name='enable' lay-filter='enable' value='" + d.id + "' lay-skin='switch' lay-text='正常|禁用'>";
                    }
                    return html;
                }
            },
            {
                field: 'id', title: '操作', width: 220,
                templet: function (d) {
                        let html = "<div>"
                        html += "<a class='pear-btn pear-btn-xs pear-btn-primary' onclick='editData(\"" + d.id + "\")'>编辑</a> " +
                            "<a class='pear-btn pear-btn-xs pear-btn-danger' onclick='deleteData(\"" + d.id + "\")'>删除</a>" +
                            "</div>";
                        return html;
                }
            },
        ]],
        page: true,
        response: {statusCode: 200},
        parseData: function (res) {
            return {
                "code": res.code,
                "msg": res.msg,
                "count": res.total,
                "data": res.data
            };
        },
        request: {
            pageName: 'pageIndex',
            limitName: 'pageSize'
        }
    });
    form.on('switch(enable)', function (data) {
        const id = this.value;
        const enable = this.checked ? 0 : 1;
        changeStatus(id, enable);
    });
}

/**
 * 编辑
 * @param id
 */
function editData(id) {
    layer.open({
        title: "编辑菜权限",
        type: 2,
        area: common.layerArea($("html")[0].clientWidth, 1000, 400),
        shadeClose: true,
        anim: 1,
        content: '/admin/permission/editPage/' + id
    });
}

/**
 *
 * @param ids
 */
function deleteData(ids) {
    layer.confirm('确定要删除吗?', {icon: 3, title: '提示'}, function (index) {
        $.ajax({
            type: "POST",
            url: "/admin/permission/del",
            contentType: "application/json",
            data: ids,
            success: function (data) {
                if (data.code === 200) {
                    queryTable();
                    parent.toast.success({message: "删除成功",position: 'topCenter'});
                } else {
                    parent.toast.error({message: data.msg,position: 'topCenter'});
                }
            },
            error: function (data) {
                parent.toast.error({message: "删除失败",position: 'topCenter'});
            }
        });
        layer.close(index);
    });
}

/**
 * 添加
 */
function add() {
    layer.open({
        title: '添加权限',
        type: 2,
        area: common.layerArea($("html")[0].clientWidth, 1000, 400),
        shadeClose: true,
        anim: 1,
        content: '/admin/permission/addPage'
    });
}

/**
 * 改变状态
 * @param id id
 * @param status status
 */
function changeStatus(id, enable) {
    $.ajax({
        type: "POST",
        url: "/admin/permission/changeStatus",
        contentType: "application/json",
        data: JSON.stringify({id: id, enable: enable}),
        success: function (data) {
            if (data.code === 200) {
                //queryTable();
                parent.toast.success({message: "修改成功",position: 'topCenter'});
            } else {
                parent.toast.error({message: data.msg,position: 'topCenter'});
            }
        },
        error: function (data) {
            parent.toast.error({message: "修改状态失败",position: 'topCenter'});
        }
    });
}
